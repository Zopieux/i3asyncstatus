#include <mpd/client.h>
#include <mpd/status.h>
#include <mpd/entity.h>
#include <mpd/search.h>
#include <mpd/tag.h>
#include <mpd/message.h>

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#define DEFAULT_FORMAT_STRING \
	"%state% [[[%artist% - ]%title%]|%file%]"

struct context_t {
	const char *status_label;
};

static struct context_t g_context;

/**
 * Reallocate the given string and append the source string.
 */
static char *
string_append(char *dest, const char *src, size_t len)
{
	size_t destlen = dest != NULL
		? strlen(dest)
		: 0;

	dest = realloc(dest, destlen + len + 1);
	memcpy(dest + destlen, src, len);
	dest[destlen + len] = '\0';

	return dest;
}

/**
 * Skip the format string until the current group is closed by either
 * '&', '|' or ']' (supports nesting).
 */
static const char *
skip_format(const char *p)
{
	unsigned stack = 0;

	while (*p != '\0') {
		if (*p == '[')
			stack++;
		else if (*p == '#' && p[1] != '\0')
			/* skip escaped stuff */
			++p;
		else if (stack > 0) {
			if (*p == ']')
				--stack;
		} else if (*p == '&' || *p == '|' || *p == ']')
			break;

		++p;
	}

	return p;
}

static char *
format_object2(const char *format, const char **last, const void *object,
	       const char *(*getter)(const void *object, const char *name))
{
	char *ret = NULL;
	const char *p;
	bool found = false;

	for (p = format; *p != '\0';) {
		switch (p[0]) {
		case '|':
			++p;
			if (!found) {
				/* nothing found yet: try the next
				   section */
				free(ret);
				ret = NULL;
			} else
				/* already found a value: skip the
				   next section */
				p = skip_format(p);
			break;

		case '&':
			++p;
			if (!found)
				/* nothing found yet, so skip this
				   section */
				p = skip_format(p);
			else
				/* we found something yet, but it will
				   only be used if the next section
				   also found something, so reset the
				   flag */
				found = false;
			break;

		case '[': {
			char *t = format_object2(p + 1, &p, object, getter);
			if (t != NULL) {
				ret = string_append(ret, t, strlen(t));
				free(t);
				found = true;
			}
		}
			break;

		case ']':
			if (last != NULL)
				*last = p + 1;
			if (!found) {
				free(ret);
				ret = NULL;
			}
			return ret;

		case '\\': {
			/* take care of escape sequences */
			char ltemp;
			switch (p[1]) {
			case 'a':
				ltemp = '\a';
				break;

			case 'b':
				ltemp = '\b';
				break;

			case 't':
				ltemp = '\t';
				break;

			case 'n':
				ltemp = '\n';
				break;

			case 'v':
				ltemp = '\v';
				break;

			case 'f':
				ltemp = '\f';
				break;

			case 'r':
				ltemp = '\r';
				break;

			case '[':
			case ']':
				ltemp = p[1];
				break;

			default:
				/* unknown escape: copy the
				   backslash */
				ltemp = p[0];
				--p;
				break;
			}

			ret = string_append(ret, &ltemp, 1);
			p += 2;
		}
			break;

		case '%': {
			/* find the extent of this format specifier
			   (stop at \0, ' ', or esc) */
			const char *end = p + 1;
			while (*end >= 'a' && *end <= 'z')
				++end;

			const size_t length = end - p + 1;

			if (*end != '%') {
				ret = string_append(ret, p, length - 1);
				p = end;
				continue;
			}

			char name[32];
			if (length > (int)sizeof(name)) {
				ret = string_append(ret, p, length);
				p = end + 1;
				continue;
			}

			memcpy(name, p + 1, length - 2);
			name[length - 2] = 0;

			const char *value = getter(object, name);
			size_t value_length;
			if (value != NULL) {
				if (*value != 0)
					found = true;
				value_length = strlen(value);
			} else {
				/* unknown variable: copy verbatim
				   from format string */
				value = p;
				value_length = length;
			}

			ret = string_append(ret, value, value_length);

			/* advance past the specifier */
			p = end + 1;
		}
			break;

		case '#':
			/* let the escape character escape itself */
			if (p[1] != '\0') {
				ret = string_append(ret, p + 1, 1);
				p += 2;
				break;
			}

			/* fall through */

		default:
			/* pass-through non-escaped portions of the format string */
			ret = string_append(ret, p, 1);
			++p;
		}
	}

	if (last != NULL)
		*last = p;
	return ret;
}

char *
format_object(const char *format, const void *object,
	      const char *(*getter)(const void *object, const char *name))
{
	return format_object2(format, NULL, object, getter);
}

static const char *
format_mtime(char *buffer, size_t buffer_size,
	     const struct mpd_song *song, const char *format)
{
	time_t t = mpd_song_get_last_modified(song);
	if (t == 0)
		return NULL;

	struct tm tm;
#ifdef WIN32
	tm = *localtime(&t);
#else
	localtime_r(&t, &tm);
#endif
	strftime(buffer, buffer_size, format, &tm);
	return buffer;
}

/**
 * Extract an attribute from a song object.
 *
 * @param song the song object
 * @param name the attribute name
 * @return the attribute value; NULL if the attribute name is invalid;
 * an empty string if the attribute name is valid, but not present in
 * the song
 */
static const char *
song_value(const struct mpd_song *song, const char *name)
{
	static char buffer[40];
	const char *value;

	if (strcmp(name, "file") == 0)
		value = mpd_song_get_uri(song);
	else if (strcmp(name, "time") == 0) {
		unsigned duration = mpd_song_get_duration(song);

		if (duration > 0) {
			snprintf(buffer, sizeof(buffer), "%u:%02u",
				 duration / 60, duration % 60);
			value = buffer;
		} else
			value = NULL;
	} else if (strcmp(name, "position") == 0) {
		unsigned pos = mpd_song_get_pos(song);
		snprintf(buffer, sizeof(buffer), "%u", pos+1);
		value = buffer;
	} else if (strcmp(name, "id") == 0) {
		snprintf(buffer, sizeof(buffer), "%u", mpd_song_get_id(song));
		value = buffer;
	} else if (strcmp(name, "mtime") == 0) {
		value = format_mtime(buffer, sizeof(buffer), song, "%c");
	} else if (strcmp(name, "mdate") == 0) {
		value = format_mtime(buffer, sizeof(buffer), song, "%x");
	} else if (strcmp(name, "state") == 0) {
		value = g_context.status_label;
	} else {
		enum mpd_tag_type tag_type = mpd_tag_name_iparse(name);
		if (tag_type == MPD_TAG_UNKNOWN)
			return NULL;
		value = mpd_song_get_tag(song, tag_type, 0);
	}

	if (value != NULL)
		// TODO
		// value = charset_from_utf8(value);
		1;
	else
		value = "";

	return value;
}

static const char *
song_getter(const void *object, const char *name)
{
	return song_value((const struct mpd_song *)object, name);
}

char *
format_song(const struct mpd_song *song, const char *format)
{
	return format_object(format, song, song_getter);
}

static int
handle_error(struct mpd_connection *conn) {
	assert(mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS);
	fprintf(stderr, "%s\n", mpd_connection_get_error_message(conn));
	mpd_connection_free(conn);
	return EXIT_FAILURE;
}

static void
fill_context(struct mpd_connection* conn, const char *label_play, const char *label_pause, const char *label_stop) {
	struct mpd_status *status = mpd_run_status(conn);
	// TODO error check
	enum mpd_state state = mpd_status_get_state(status);
	mpd_status_free(status);
	if (state == MPD_STATE_PLAY)
		g_context.status_label = label_play;
	else if (state == MPD_STATE_PAUSE)
		g_context.status_label = label_pause;
	else
		g_context.status_label = label_stop;
}

int
main(int argc, char *const *argv) {

	char *format_string = DEFAULT_FORMAT_STRING;
	char *label_play = ">";
	char *label_pause = "||";
	char *label_stop = "[]";

	int c;
	while ((c = getopt(argc, argv, "f:p:P:s")) != -1) {
		switch (c) {
		case 'f':
			format_string = optarg;
			break;
		case 'p':
			label_play = optarg;
			break;
		case 'P':
			label_pause = optarg;
			break;
		case 's':
			label_stop = optarg;
			break;
		case '?':
		defaut:
			return EXIT_FAILURE;
			break;
		}
	}

	g_context.status_label = "";
	setlinebuf(stdout);

	struct mpd_connection *conn;
	conn = mpd_connection_new(NULL, 0, 30000);

	if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
		return handle_error(conn);

	enum mpd_idle mask;
	struct mpd_song *song;
	char *formatted;

	song = mpd_run_current_song(conn);
	fill_context(conn, label_play, label_pause, label_stop);
	formatted = format_song(song, format_string);
	mpd_song_free(song);
	printf("%s\n", formatted);
	free(formatted);

	while (1) {
		mask = mpd_run_idle_mask(conn, MPD_IDLE_PLAYER | MPD_IDLE_MIXER);
		if (mask == 0)	
			return handle_error(conn);
		song = mpd_run_current_song(conn);
		if (song == NULL) {
			if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS)
				return handle_error(conn);
			continue;
		}
		fill_context(conn, label_play, label_pause, label_stop);
		formatted = format_song(song, format_string);
		mpd_song_free(song);
		printf("%s\n", formatted);
		free(formatted);
	}

	mpd_connection_free(conn);
	return EXIT_SUCCESS;
}

