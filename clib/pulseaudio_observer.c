#include <pulse/pulseaudio.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <getopt.h>
#include <signal.h>

#ifdef DEBUG
#define DLOG(fmt, ...) do { \
	    fprintf(stderr, "[%s:%d] DEBUG: " fmt, __FILE__, __LINE__, ##__VA_ARGS__); \
} while(0)
#else
#define DLOG(fmt, ...) do { } while(0)
#endif
#define CLIENT_NAME "pulse_volume_watcher"
#define PA_ERROR(context) pa_strerror(pa_context_errno(context))

enum conn_state_t {CONN_WAIT, CONN_SUCCESS, CONN_FAILED};
static pa_volume_t (*calculator)(const pa_cvolume *vol);
static pa_threaded_mainloop *main_loop;
static enum conn_state_t conn_state = CONN_WAIT;
static int running = 1;
static uint32_t sink_index = 0;
static pa_volume_t old_volume = 0, current_volume = 0;

static void
z_context_state_callback(pa_context *context, void *user_data) {
	pa_context_state_t state = pa_context_get_state(context);
	switch (state) {
	case PA_CONTEXT_READY:
		conn_state = CONN_SUCCESS;
		break;
	case PA_CONTEXT_FAILED:
	case PA_CONTEXT_TERMINATED:
		conn_state = CONN_FAILED;
		break;
	default:
		break;
	}
	pa_threaded_mainloop_signal(main_loop, 0);
}

static void
z_sink_info_callback(pa_context *context, const pa_sink_info *info, int eol, void *userdata) {
	if (!info && eol < 0) {
		DLOG("error in sink_info\n");
		return;
	}
	if (eol)
		return;
	if (!pa_cvolume_valid(&info->volume)) {
		DLOG("volume is invalid\n");
		return;
	}
	DLOG("event received\n");
	if (info->mute || pa_cvolume_is_muted(&info->volume)) {
		DLOG("muted\n");
		current_volume = 0;
	} else
		current_volume = calculator(&info->volume);
	pa_threaded_mainloop_signal(main_loop, 0);
}

static void
z_state_subscribe_callback(pa_context *context, pa_subscription_event_type_t type, uint32_t index, void *user_data) {
	pa_subscription_event_type_t type_mask = type & PA_SUBSCRIPTION_EVENT_TYPE_MASK;
	pa_subscription_event_type_t facility_mask = type & PA_SUBSCRIPTION_EVENT_FACILITY_MASK;
	
	if (type_mask == PA_SUBSCRIPTION_EVENT_CHANGE && facility_mask == PA_SUBSCRIPTION_EVENT_SINK)
		sink_index = index;	
	pa_threaded_mainloop_signal(main_loop, 0);
}

static void
sighandler(int sig) {
	running = 0;
	pa_threaded_mainloop_signal(main_loop, 0);
}

int
main(int argc, char * const* argv) {
	static const char* usage_str = "Usage: %s [-m min|max|avg] (default is avg)\n";
	int opt;

	calculator = pa_cvolume_avg;	

	while ((opt = getopt(argc, argv, "m:")) != -1) {
		if (opt == 'm') {
			if (strcmp(optarg, "min") == 0) {
				calculator = pa_cvolume_min;
			} else if (strcmp(optarg, "max") == 0) {
				calculator = pa_cvolume_max;
			} else if (strcmp(optarg, "avg") == 0) {
				calculator = pa_cvolume_avg;
			} else {
				fprintf(stderr, usage_str, argv[0]);
				return 1;
			}
		} else {
			fprintf(stderr, usage_str, argv[0]);
			return 1;
		}
	}

	pa_mainloop_api *api;
	pa_context *context;
	pa_operation *operation;	

	if (!(main_loop = pa_threaded_mainloop_new())) {
		perror("pa_threaded_mainloop_new");
		return 2;
	}

	api = pa_threaded_mainloop_get_api(main_loop);

	if (!(context = pa_context_new(api, CLIENT_NAME))) {
		perror("pa_context_new");
		return 3;
	}

	pa_context_set_state_callback(context, z_context_state_callback, main_loop);

	if (pa_context_connect(context, NULL, PA_CONTEXT_NOAUTOSPAWN, NULL) < 0) {
		perror("pa_context_connect");
		return 4;
	}

	// let's connect
	if (pa_threaded_mainloop_start(main_loop) < 0) {
		perror("pa_threaded_mainloop_start");
		DLOG("%s\n", PA_ERROR(context));
		return 5;
	}

	pa_threaded_mainloop_lock(main_loop);
	while (conn_state == CONN_WAIT) {
		pa_threaded_mainloop_wait(main_loop);
	}
	pa_threaded_mainloop_unlock(main_loop);

	if (conn_state == CONN_FAILED) {
		perror("connection failed");
		pa_context_unref(context);
		pa_threaded_mainloop_stop(main_loop);
		pa_threaded_mainloop_free(main_loop);
		return 6;
	}

	operation = pa_context_get_sink_info_list(context, z_sink_info_callback, NULL);	
	while (pa_operation_get_state(operation) == PA_OPERATION_RUNNING)
		pa_threaded_mainloop_wait(main_loop);
	pa_operation_unref(operation);
	old_volume = current_volume;
	printf("%u\n\n", current_volume);
	fflush(stdout);

	// let's subscribe
	pa_context_set_subscribe_callback(context, z_state_subscribe_callback, NULL);

	pa_threaded_mainloop_lock(main_loop);
	signal(SIGINT, sighandler);

	while (running) {
		operation = pa_context_subscribe(context, PA_SUBSCRIPTION_MASK_SINK, NULL, NULL);
		while (pa_operation_get_state(operation) == PA_OPERATION_RUNNING)
			pa_threaded_mainloop_wait(main_loop);
		pa_operation_unref(operation);
		// we got the sink index, we can retrieve volume
		operation = pa_context_get_sink_info_by_index(context, sink_index, z_sink_info_callback, NULL);
		while (pa_operation_get_state(operation) == PA_OPERATION_RUNNING)
			pa_threaded_mainloop_wait(main_loop);
		pa_operation_unref(operation);
		if (old_volume != current_volume) { 
			old_volume = current_volume;
			printf("%u\n", current_volume);
			fflush(stdout);
		}
	}
	DLOG("interrupted\n");
	pa_threaded_mainloop_unlock(main_loop);

	pa_context_unref(context);
	pa_threaded_mainloop_stop(main_loop);
	pa_threaded_mainloop_free(main_loop);

	return 0;
}
