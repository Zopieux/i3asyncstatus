# i3asyncstatus

A i3bar compatible status that uses Python ≥3.4 asyncio capabilites.

## Building

This Python program comes with a few C adapters that listen for changes in a variety of programs.

    $ cd clib && make

### mpd_observer

Small wrapper around libmpdclient C library to retrieve current song and state using mpd "idle" API (event-oriented).

Dependencies: **libmpdclient**

### pulseaudio_observer

Small wrapper around libpulse C library to retrieve the master volume when it changes.

Dependencies: **libpulse**

## Running

For the moment there is no real main launcher. Please manually edit `test.py`. Then:

    $ python test.py

Of course you can also use i3bar `status_command`:

```nginx
bar {
        status_command /path/to/test.py
}
```
