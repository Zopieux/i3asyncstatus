from i3asyncstatus.blocks import clib_path, Message, BlockingProcessBlock


class PulseaudioBlock(BlockingProcessBlock):

    def __init__(self, context, func=None):
        cmd = [clib_path('pulseaudio_observer')]
        if func is not None:
            cmd.extend(['-m', func])
        super().__init__(context, cmd)

    def send(self, msg, priority=100):
        volume = int(int(msg) / (1 << 16) * 100)
        char = "\uf026" if volume == 0 else "\uf027" if volume < 50 else "\uf028"
        volume = Message("{}{:>4d}".format(char, volume), width="\uf028 100")
        super().send(volume, priority=priority)
