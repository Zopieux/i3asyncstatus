from i3asyncstatus.blocks import Message, BlockingProcessBlock
import re
import subprocess
import sys


class BatteryBlock(BlockingProcessBlock):
    REG_STATUS = re.compile(r'^(?P<type>adapter|battery)\s+(?P<id>[0-9]+):\s+(?P<data>.*)$', re.I)

    def __init__(self, context, ac_id=None, battery_id=None):
        cmd = ['/usr/bin/acpi_listen']
        self.ac_id = int(ac_id) if ac_id is not None else None
        self.battery_id = int(battery_id) if battery_id is not None else None
        self.on_line = None
        self.percent = None
        self.full = None
        super().__init__(context, cmd)

    def setup(self):
        self._get_state()

    def _formated(self):
        if self.full and self.on_line:
            return None
        state_char = "\uf1e6" if self.on_line else "\uf3cf"
        msg = "{} {:3>d}%".format(state_char, self.percent)
        return Message(msg, color="#a5c261" if self.on_line else "#da493", align="right")

    def _get_state(self):
        changed = False
        state = subprocess.check_output(['/usr/bin/acpi', '--ac-adapter', '--battery'])
        for line in state.decode('utf8').split('\n'):
            match = self.REG_STATUS.match(line)
            if match is None:
                continue
            ev_type = match.group('type').strip().lower()
            ev_id = int(match.group('id'))
            content = match.group('data')
            if ev_type == 'adapter' and (self.ac_id is None or ev_id == self.ac_id):
                self.on_line = 'on-line' in line
                changed = True
            elif ev_type == 'battery' and (self.battery_id is None or ev_id == self.battery_id):
                try:
                    state, perc = content.split(',', 2)
                    self.percent = int(perc.replace('%', ''))
                    self.full = state.strip().lower() == 'full'
                    changed = True
                except ValueError:
                    pass
        if changed and self.on_line is not None and self.full is not None and self.percent is not None:
            self.send(self._formated())

    def read_stdout(self, data):
        first_word = data.split(None, 1)[0].lower()
        if first_word in ('ac_adapter', 'battery'):
            self._get_state()
