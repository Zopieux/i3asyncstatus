import asyncio
import os


def clib_path(*parts):
    root = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    return os.path.join(root, 'clib', *parts)


class Message:

    def __init__(self, content=None, short=None, color=None, urgent=False,
                 width=None, align=None, separator=True, separator_width=None):
        self.content = content
        self.short = short
        self.color = color
        self.urgent = urgent
        self.width = width
        self.align = align
        self.separator = separator
        self.separator_width = separator_width

    def __bool__(self):
        return bool(self.content) or bool(self.short)

    def value(self):
        data = {'full_text': self.content}
        if self.short:
            data['short_text'] = self.short
        if self.color:
            data['color'] = self.color
        if self.width:
            try:
                data['min_width'] = int(self.width)
            except ValueError:
                data['min_width'] = self.width
        if self.align and self.align in ('center', 'left', 'right'):
            data['align'] = self.align
        if self.urgent:
            data['urgent'] = True
        if not self.separator:
            data['separator'] = False
        if self.separator_width:
            try:
                data['separator_block_width'] = int(self.separator_width)
            except ValueError:
                pass
        return data


class BaseBlock:

    def __init__(self, context):
        self.context = context

    def setup(self):
        # Default implementation does no setup
        pass

    def teardown(self):
        # Default implementation does no setup
        pass

    def run(self, uid):
        self._uid = uid
        self.setup()

    @property
    def loop(self):
        return self.context.loop

    def send(self, *parts, priority=100):
        if not parts or not any(parts):
            self.context.messages.append(self._uid, [], priority)
            return
        self.context.messages.append(
            self._uid, [
                part if isinstance(
                    part, Message) else Message(part) for part in parts], priority)


class _ProcessBlock(BaseBlock):

    class SubprocessProtocol(asyncio.SubprocessProtocol):

        def __init__(self, exit_future, parent):
            self.exit_future = exit_future
            self.parent = parent

        def pipe_data_received(self, fd, data):
            data = data.decode('utf8').strip()
            if fd == 1:
                self.parent.read_stdout(data)
            else:
                self.parent.read_stderr(data)

        def process_exited(self):
            if not self.exit_future.cancelled():
                self.exit_future.set_result(True)

    def read_stdout(self, data):
        self.send(data)

    def read_stderr(self, data):
        self.send_error("stderr: %s" % data)

    def send_error(self, msg):
        cmd = " ".join(self._cmd)
        if len(cmd) > 16:
            cmd = cmd[:16] + "…"
        self.send(Message("%s: %s" % (cmd, msg), color='#ff0000', urgent=True), priority=10)


class PeriodicProcessBlock(_ProcessBlock):

    def __init__(self, context, cmd, interval, timeout=None, max_tries=3):
        super().__init__(context)
        self._cmd = cmd
        self._interval = interval
        self._timeout = timeout
        self._max_tries = max_tries

    @asyncio.coroutine
    def run(self, uid):
        super().run(uid)
        errors = 0
        tokill = []
        while True:
            exit_future = asyncio.Future(loop=self.loop)
            proc = self.loop.subprocess_exec(lambda: _ProcessBlock.SubprocessProtocol(exit_future, self),
                                             *self._cmd, stdin=asyncio.subprocess.DEVNULL)
            transport, protocol = yield from proc
            try:
                yield from asyncio.wait_for(exit_future, self._timeout)
            except asyncio.TimeoutError:
                errors += 1
                tokill.append(transport)
                self.send_error("process time-out (%d tries)" % errors)
                if errors >= self._max_tries:
                    break
            yield from asyncio.sleep(self._interval)

        self.send_error("process exited after %d tries" % errors)
        for transport in tokill:
            try:
                if transport.get_returncode() is None:
                    transport.terminate()
                    if transport.get_returncode() is None:
                        yield from asyncio.sleep(2)
                        transport.kill()
            except Exception:
                pass
            self.send_error("process killed (%d)" % transport.get_returncode())


class BlockingProcessBlock(_ProcessBlock):

    def __init__(self, context, cmd):
        super().__init__(context)
        self._cmd = cmd

    @asyncio.coroutine
    def run(self, uid):
        super().run(uid)
        try:
            exit_future = asyncio.Future(loop=self.loop)
            proc = self.loop.subprocess_exec(lambda: _ProcessBlock.SubprocessProtocol(exit_future, self),
                                             *self._cmd, stdin=asyncio.subprocess.DEVNULL)
            transport, protocol = yield from proc
            yield from exit_future
            transport.close()
        except Exception:
            pass
        self.send_error("Process exited (%d)" % transport.get_returncode())
