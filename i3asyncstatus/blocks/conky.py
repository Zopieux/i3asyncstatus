from i3asyncstatus.blocks import Message, BlockingProcessBlock
import json


class ConkyBlock(BlockingProcessBlock):

    def __init__(self, context, config=None, use_json=False):
        cmd = ['/usr/bin/conky']
        self._use_json = use_json
        if config is not None:
            cmd.extend(['-c', config])
        super().__init__(context, cmd)

    def send(self, msg, priority=100):
        if self._use_json:
            parts = []
            for block in json.loads(msg):
                if isinstance(block, dict):
                    content = block.pop('text', None)
                    parts.append(Message(content, **block))
                else:
                    parts.append(Message(block))
            super().send(*parts, priority=priority)
        else:
            super().send(msg, priority=priority)
