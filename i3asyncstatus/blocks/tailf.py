import asyncio
import os


@asyncio.coroutine
def read_pipe(fobj):
    loop = asyncio.get_event_loop()
    stream_reader = asyncio.StreamReader(loop=loop)

    def factory():
        return asyncio.StreamReaderProtocol(stream_reader)
    transport, _ = yield from loop.connect_read_pipe(factory, fobj)
    return stream_reader, transport


class Tailf:

    def __init__(self, filename):
        self._fileobj = open(filename)

    @asyncio.coroutine
    def update(self):
        out, transport = yield from read_pipe(self._fileobj)
        return out.readline()
