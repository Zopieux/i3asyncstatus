from i3asyncstatus.blocks import clib_path, Message, BlockingProcessBlock


class MpdBlock(BlockingProcessBlock):

    def __init__(self, context, format=None, label_play=None, label_pause=None, label_stop=None):
        cmd = [clib_path('mpd_observer')]
        if format is not None:
            cmd.extend(['-f', format])
        if label_play is not None:
            cmd.extend(['-p', label_play])
        if label_pause is not None:
            cmd.extend(['-P', label_pause])
        if label_stop is not None:
            cmd.extend(['-s', label_stop])
        super().__init__(context, cmd)
