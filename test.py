import asyncio
import collections
import json
import os

import i3asyncstatus.blocks.mpd
import \
    i3asyncstatus.blocks.pulseaudio, \
    i3asyncstatus.blocks.conky, \
    i3asyncstatus.blocks.battery
import i3asyncstatus.blocks as blocks


class BlockContext:

    def __init__(self, loop, messages):
        self.loop = loop
        self.messages = messages


class MessageQueue(asyncio.PriorityQueue):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def append(self, sender, msg, priority=100):
        self.put_nowait((priority, (sender, msg)))


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    incoming_messages = MessageQueue(loop=loop)
    context = BlockContext(loop, incoming_messages)
    incoming_lock = asyncio.Lock(loop=loop)
    outgoing_lock = asyncio.Lock(loop=loop)
    messages = collections.OrderedDict()

    @asyncio.coroutine
    def consume_events():
        while True:
            needs_update = False
            prio, (sender, parts) = yield from incoming_messages.get()
            if sender is None:
                raise ValueError("Received a None sender")
            with (yield from incoming_lock):
                if messages[sender] != parts:
                    messages[sender] = parts
                    needs_update = True
            if needs_update:
                with (yield from outgoing_lock):
                    all_parts = []
                    for parts in messages.values():
                        all_parts.extend(p.value() for p in parts if p)
                    print(json.dumps(all_parts), end=",\n", flush=True)

    tasks = []

    tasks.append(blocks.mpd.MpdBlock(context, label_play='\uf04b', label_pause='\uf04c'))
    tasks.append(blocks.conky.ConkyBlock(context, config=os.path.expanduser("~/.config/conkyrc"), use_json=True))
    tasks.append(blocks.battery.BatteryBlock(context))
    tasks.append(blocks.pulseaudio.PulseaudioBlock(context))
    tasks.append(blocks.PeriodicProcessBlock(context, ['date', '+\uf133 %b %d \uf017 %T'], 5))

    for uid, task in enumerate(tasks):
        messages[uid] = [blocks.Message()]
        loop.create_task(task.run(uid))

    loop.create_task(consume_events())

    os.nice(1)

    print(json.dumps({'version': 1}), end="\n[\n", flush=True)
    loop.run_forever()
    loop.close()
